package com.insurance.domain;

import java.math.BigDecimal;

import com.insurance.domain.enums.CarType;

public class Car {

	private CarType carType;
	private boolean antiTheftDevice;
	private BigDecimal value;
	private String vehicleIdentificationNumber;
	
	
	public Car(CarType carType, boolean antiTheftDevice, BigDecimal value, String vehicleIdentificationNumber) {
		super();
		this.carType = carType;
		this.antiTheftDevice = antiTheftDevice;
		this.value = value;
		this.vehicleIdentificationNumber = vehicleIdentificationNumber;
	}


	public CarType getCarType() {
		return carType;
	}


	public void setCarType(CarType carType) {
		this.carType = carType;
	}


	public boolean isAntiTheftDevice() {
		return antiTheftDevice;
	}


	public void setAntiTheftDevice(boolean antiTheftDevice) {
		this.antiTheftDevice = antiTheftDevice;
	}


	public BigDecimal getValue() {
		return value;
	}


	public void setValue(BigDecimal value) {
		this.value = value;
	}


	public String getVehicleIdentificationNumber() {
		return vehicleIdentificationNumber;
	}


	public void setVehicleIdentificationNumber(String vehicleIdentificationNumber) {
		this.vehicleIdentificationNumber = vehicleIdentificationNumber;
	}
}
