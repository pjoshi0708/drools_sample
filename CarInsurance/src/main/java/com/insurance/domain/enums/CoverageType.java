package com.insurance.domain.enums;

public enum CoverageType {
	/**
     * Accidents 
     */ 
    COLLISION, 
    /**
     * Fire, theft, storm, flood, vandalism, ... 
     */ 
    COMPREHENSIVE, 
    /**
     * Bodily injury 
     */ 
    BODILY_INJURY_LIABILITY, 
    /**
     * Property damage 
     */ 
    PROPERTY_DAMAGE_LIABILITY, 
    /**
     * towing, battery jump-start, flat tire change, ... 
     */ 
    ROADSIDE_ASSISTANCE 
}
