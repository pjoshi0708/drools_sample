package com.insurance.domain.enums;

public enum CarType {
	/**
     * Low risk for theft, high risk for injuries inside. 
     */ 
    SMALL, 
    /**
     * Normal risk 
     */ 
    MEDIUM, 
    /**
     * High risk for theft, low risk for injuries inside, high risk for injuries outside. 
     */ 
    LARGE, 
    /**
     * High risk for everything 
     */ 
    SPORT, 
    /**
     * High risk for everything 
     */ 
    MUSCLE, 
    /**
     * High risk for repair costs. 
     */ 
    LUXURY 
}
