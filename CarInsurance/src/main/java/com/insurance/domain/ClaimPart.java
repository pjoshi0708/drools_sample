package com.insurance.domain;

import java.math.BigDecimal;

import com.insurance.domain.enums.CoverageType;

public class ClaimPart {
	
	private Claim claim; 
    private CoverageType coverageType; 
    private BigDecimal damagesCost;
    
	public Claim getClaim() {
		return claim;
	}
	public void setClaim(Claim claim) {
		this.claim = claim;
	}
	public CoverageType getCoverageType() {
		return coverageType;
	}
	public void setCoverageType(CoverageType coverageType) {
		this.coverageType = coverageType;
	}
	public BigDecimal getDamagesCost() {
		return damagesCost;
	}
	public void setDamagesCost(BigDecimal damagesCost) {
		this.damagesCost = damagesCost;
	}
}
