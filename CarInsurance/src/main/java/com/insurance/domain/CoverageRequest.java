package com.insurance.domain;

import java.util.ArrayList;
import java.util.List;

import com.insurance.domain.enums.CoverageType;

public class CoverageRequest {

	private PolicyRequest policyRequest; 
    private CoverageType coverageType; 
    private boolean automaticallyDisapproved = false; 
    private List<String> disapprovalMessageList = new ArrayList<String>();
	public PolicyRequest getPolicyRequest() {
		return policyRequest;
	}
	public void setPolicyRequest(PolicyRequest policyRequest) {
		this.policyRequest = policyRequest;
	}
	public CoverageType getCoverageType() {
		return coverageType;
	}
	public void setCoverageType(CoverageType coverageType) {
		this.coverageType = coverageType;
	}
	public boolean isAutomaticallyDisapproved() {
		return automaticallyDisapproved;
	}
	public void setAutomaticallyDisapproved(boolean automaticallyDisapproved) {
		this.automaticallyDisapproved = automaticallyDisapproved;
	}
	public List<String> getDisapprovalMessageList() {
		return disapprovalMessageList;
	}
	public void setDisapprovalMessageList(List<String> disapprovalMessageList) {
		this.disapprovalMessageList = disapprovalMessageList;
	} 
	
	public void addDisapprovalMessage(String disapprovalMessage) { 
        disapprovalMessageList.add(disapprovalMessage); 
    } 
}
