package com.insurance.domain;

import java.math.BigDecimal;
import com.insurance.domain.enums.CoverageType;

public class Coverage {
	
	private Policy policy; 
    private CoverageType coverageType; 
    private BigDecimal coverageLimit; 
    private BigDecimal coverageCost;
    
	public Policy getPolicy() {
		return policy;
	}
	public void setPolicy(Policy policy) {
		this.policy = policy;
	}
	public CoverageType getCoverageType() {
		return coverageType;
	}
	public void setCoverageType(CoverageType coverageType) {
		this.coverageType = coverageType;
	}
	public BigDecimal getCoverageLimit() {
		return coverageLimit;
	}
	public void setCoverageLimit(BigDecimal coverageLimit) {
		this.coverageLimit = coverageLimit;
	}
	public BigDecimal getCoverageCost() {
		return coverageCost;
	}
	public void setCoverageCost(BigDecimal coverageCost) {
		this.coverageCost = coverageCost;
	} 
}
