package com.insurance.domain;

import java.util.ArrayList;
import java.util.List;

public class PolicyRequest {
	
	private Driver owner; 
    private Car car; 
    private List<CoverageRequest> coverageRequestList = new ArrayList<CoverageRequest>(); 
    private boolean automaticallyRejected = false; 
    private List<String> rejectedMessageList = new ArrayList<String>(); 
    private List<String> flaggedMessageList = new ArrayList<String>(); 
    private boolean requiresManualApproval = false; 
    private boolean manuallyApproved = false;
    
    
	public Driver getOwner() {
		return owner;
	}
	public void setOwner(Driver owner) {
		this.owner = owner;
	}
	public Car getCar() {
		return car;
	}
	public void setCar(Car car) {
		this.car = car;
	}
	public List<CoverageRequest> getCoverageRequestList() {
		return coverageRequestList;
	}
	public void setCoverageRequestList(List<CoverageRequest> coverageRequestList) {
		this.coverageRequestList = coverageRequestList;
	}
	public boolean isAutomaticallyRejected() {
		return automaticallyRejected;
	}
	public void setAutomaticallyRejected(boolean automaticallyRejected) {
		this.automaticallyRejected = automaticallyRejected;
	}
	public List<String> getRejectedMessageList() {
		return rejectedMessageList;
	}
	public void setRejectedMessageList(List<String> rejectedMessageList) {
		this.rejectedMessageList = rejectedMessageList;
	}
	public List<String> getFlaggedMessageList() {
		return flaggedMessageList;
	}
	public void setFlaggedMessageList(List<String> flaggedMessageList) {
		this.flaggedMessageList = flaggedMessageList;
	}
	public boolean isRequiresManualApproval() {
		return requiresManualApproval;
	}
	public void setRequiresManualApproval(boolean requiresManualApproval) {
		this.requiresManualApproval = requiresManualApproval;
	}
	public boolean isManuallyApproved() {
		return manuallyApproved;
	}
	public void setManuallyApproved(boolean manuallyApproved) {
		this.manuallyApproved = manuallyApproved;
	}
    
	public void addCoverageRequest(CoverageRequest coverageRequest) { 
        coverageRequest.setPolicyRequest(this); 
        coverageRequestList.add(coverageRequest); 
    } 
 
    public void addRejectedMessage(String rejectedMessage) { 
        rejectedMessageList.add(rejectedMessage); 
    } 
 
    public void addFlaggedMessage(String flaggedMessage) { 
        flaggedMessageList.add(flaggedMessage); 
    } 
 
    public boolean isApproved() { 
        return !automaticallyRejected && (!requiresManualApproval || manuallyApproved); 
    } 
}
